---
layout: page
title: About
permalink: /about/
---

SARAH GUILFORD (434) 282-6021 | | guilford.sarah@gmail.com
w w w . l i n k e d i n . c o m / i n / s a r a h - g u i l fo r d 1
Versatile, established non profit professional with experience in arts administration and digital marketing . Expertise lies in design, writing, public relations, and social media. Practiced in adaptive design and marketing techniques and styles to suit a specific audience. More than prepared to offer developing skills in market analysis and communication strategy to see projects from inception to completion. Eager to add value in accordance with organizational objectives while achieving concentrated results for the company.
 NM a a n n t c u h c e k s t e e t r , , MN HA Simsbury, CT
    • • •
Marketing Campaigns Social Media Management Consumer Behavior
• Database Management • Program Development • Project Coordination
•
• Press Releases
Event Planning
• Content and Product Design
  Technical Skills: Versai Museum Software | Tessitura Ticketing and Donor Software| Adobe Suite | Microsoft Suite | Google Suite| Affinity Publisher| Constant Contact | MailChimp | Squarespace | DocuSign | Dropbox | Scribus | Indesign | HTML | SQL
PROFESSIONAL EXPERIENCE
RIGHT ANGLE ENTERTAINMENT | LOS ANGELES, CA (Contract, Remote)
Marketing Manager
SEP 2021 – JAN 2022 Managed national and Canadian marketing efforts for several touring shows including The Price is Right Live!, Friends: A Musical
Parody, Rudolph the Red-Nosed Reindeer: The Musical.
• Coordinated interviews and social media promotions between talent and market venues, radio stations, magazines, and blogs.
• Utilized programs like Affinity Publisher and Canva to create social media content, pitch decks, and product designs.
• Edited, proofread, and updated show programs. Collected and organized headshots, biographies, and other talent/crew
information for efficient program updating.
• Wrote and distributed press releases for upcoming events and shows.
• Collected and filed advertisement plans from venues.
• Drafted weekly ticket sales reports.
• Built website for new event project using Squarespace. Wrote copy for event website to drive traffic to purchase tickets for immersive movie experience.
   THE ARTISTS ASSOCIATION OF NANTUCKET | NANTUCKET, MA
Marketing and Development Associate
 Created and implemented social media campaigns for Facebook, Instagram, Twitter. Utilize Versai Museum Software in Development tasks such as running lists and reports as well as tracking Artist and Patron memberships.
•
• •
Created and implemented all print advertisement schedules while maintaining relationships with local newspapers, newsletters, radio stations
Edited documents and images as needed in Adobe Suite, assisting development director and other departments as needed.
Analyzeed market to understand key market trends and opportunities, participated in creating and implementing marketing and branding campaigns, and examining the impact of the campaign.
THE PARAMOUNT THEATER | CHARLOTTESVILLE, VA
Education and Outreach Assistant
JAN 2020 – APR 2020
 Collaborated with Central Virginia public, private, and home school teachers to register and submit financial assistance requests for the Arts Education Program.
• Consistently ran a variety of reports using Tessitura ticketing software for Arts Education performances to keep track of payments and registration numbers.
• Scheduled and sent payment reminders and show day info, coordinate the registration process with Box Office Staff.
Sarah Guilford | 1
MAY 2018 – DEC 2019
• Worked closely with the Education and Outreach Manager in various program duties, updated education and outreach operations manual, assisted in tracking outreach tickets with local nonprofit organizations, and created survey compilations from teacher surveys following each education performance.
• Assisted during show days with volunteers and seating.
• Worked with parents to register their kids for the annual theater summer camp.
THE PARAMOUNT THEATER | CHARLOTTESVILLE, VA
Box Office Associate
AUG 2017 – MAY 2018
 Provided customer service to ticket buyers over the phone and in person using Tessitura ticketing software during Box Office hours as well as events throughout the year.
• Worked with development department in ensuring sponsor tickets are processed, reported and tracked, settled daily cash and checked batches using Tessitura and reported to finance department.
• Settled and balanced cash drawer and ensured cash sales matched batches. Guaranteed tickets are
provided to patron via their preferred method of Mail, Will-Call, E-Mail, or SMS, in addition to changing the marquee according to schedule.
• Participated in volunteer recycle program, showing the community that Charlottesville cares about the environment by assisting with marketing initiatives and research.
• Processed and recorded donation requests in coordination with the Box Office Manager. CHARLOTTESVILLE OPERA | CHARLOTTESVILLE, VA
Interim Assistant to the Executive and Artistic Directors
NOV 2017 – MAY 2018
 Created budgets for 2018 marketing and advertising, working with local newspapers, magazines, and radio groups to reflect budget,
working closely with members of the board, sharing all important materials and information of upcoming events/programs as well as set up for board meetings.
• Managed and updated all social media (Facebook, Twitter, Instagram) and website using Squarespace.
• Created and recorded artist contracts and W-9’s.
• Organized the Artist in Residence Program in 2018, working with schools and teachers in signing up for sessions, creating all
educational material to be shared with students and teachers, organizing volunteer schedule.
• Processed donations, creating and recording gift certificates (tickets donations) and acknowledgement letters.
• Drafted and distributed e-blasts using Mailchimp to keep constituents up to date on upcoming events.
• Assisted in event logistics, including book venues, solicit food donations, create and track invitations using paperless post, create
programs.
THE ISHAN GALA CHILDREN’S CANCER FOUNDATION | CHARLOTTESVILLE, VA
Events Intern
SEPT 2017 – JAN 2018
 Managed set-up for events like the Annual Holiday Cheer Program, managing event activities such as raffle drawing prizes, and sponsor booths.
• Handled other event logistics such as venue and vendor relationships.
• Worked with Development Intern on marketing initiatives, manage social media posts, created and distributed press releases.
CHARLOTTESVILLE OPERA | CHARLOTTESVILLE, VA
Arts Administration Intern
JAN 2017 – AUG 2017 Assisted in 2017 playbill compilation, compile artist information, selling ads for the 2017 playbill, more than doubling revenue from
previous year.
• Arranged trade agreements with local businesses, creating and running supertitles, in The Paramount Theater for the opera’s first
premiere in 35 years in collaboration with The Virginia Festival of the Book.
• Created educational materials and programs for upcoming events, in addition to executing marketing initiatives.
• Aided with events, performances, and rehearsals.
 EDUCATION
Master of Arts Candidate, Culture Management| Rome Business School Bachelor of Music, Vocal Performance | The Boston Conservatory
Volunteer, Ran Surtitles for Victory Hall Opera’s October 2017 | Production, Sympathy.
Expected to graduate 2022 2014
